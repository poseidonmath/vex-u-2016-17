#pragma config(I2C_Usage, I2C1, i2cSensors)
#pragma config(Motor,  port1,           ARM_RIGHT1,    tmotorVex393_HBridge, openLoop, encoderPort, None)
#pragma config(Motor,  port2,           ARM_RIGHT2,    tmotorVex393_MC29, openLoop, encoderPort, None)
#pragma config(Motor,  port3,           ARM_RIGHT3,    tmotorVex393_MC29, openLoop, encoderPort, None)
#pragma config(Motor,  port4,           ARM_LEFT1,     tmotorVex393_MC29, openLoop, encoderPort, None)
#pragma config(Motor,  port5,           ARM_LEFT2,     tmotorVex393_MC29, openLoop, encoderPort, None)
#pragma config(Motor,  port6,           ARM_LEFT3,     tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port7,           FRONT_LEFT,    tmotorVex393_MC29, openLoop, encoderPort, None)
#pragma config(Motor,  port8,           BACK_LEFT,     tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port9,						FRONT_RIGHT,	 tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port10,          BACK_RIGHT,    tmotorVex393_MC29, openLoop)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

// Main Vex Bot Code
signed char filterJoystickAxis(signed char axis){
	return (axis < 25 && axis > 25)? 0:pow(axis, 3);
}
void DriveTrain(signed char left,signed char right, signed char mid){
		motor[FRONT_LEFT] = left;
		motor[BACK_LEFT] = left;
		//motor[LEFT_DRIVE2] = left;
		motor[FRONT_RIGHT] = right;
		motor[BACK_RIGHT] = right;
		//motor[RIGHT_DRIVE2] = right;
		//motor[MID] = mid;
}

void tankDrive(){
	//Grab values from joystick axes
	signed char	left_vector = vexRT[Ch3];
	signed char right_vector = -vexRT[Ch2];
	//Test to see if the values are in the dead zone, otherwise cube the vector
	filterJoystickAxis(left_vector);
	filterJoystickAxis(right_vector);
	//Drive the motors in tank fashion
	DriveTrain(left_vector,right_vector, left_vector);

}

void arcadeDrive(){
	//Grab values from joystick axes
 	signed char foward_vector = (vexRT[Ch3]);
	signed char rotational_vector = (vexRT[Ch1]);
	signed char strafe_vector = (vexRT[Ch4]);
	//Test to see if the values are in the dead zone, otherwise cube the vector
	filterJoystickAxis(foward_vector);
	filterJoystickAxis(rotational_vector);
	filterJoystickAxis(strafe_vector);
	//Drive using arcade fashion
	DriveTrain(foward_vector + rotational_vector,foward_vector - rotational_vector, strafe_vector);
}
void shooter(){
	char button = vexRT[Btn5DXmtr2];
	char buttons = vexRT[Btn6DXmtr2];
	if(vexRT[Btn6DXmtr2]) {
		motor[ARM_LEFT1] =  -buttons*127;
		motor[ARM_LEFT2] =  -buttons*127;
		motor[ARM_LEFT3] =  -buttons*127;

		motor[ARM_RIGHT1] = buttons*127;
		motor[ARM_RIGHT2] = buttons*127;
		motor[ARM_RIGHT3] = buttons*127;
	}
	else {
		motor[ARM_LEFT1] =  button*127;
		motor[ARM_LEFT2] =  button*127;
		motor[ARM_LEFT3] =  button*127;

		motor[ARM_RIGHT1] = -button*127;
		motor[ARM_RIGHT2] = -button*127;
		motor[ARM_RIGHT3] = -button*127;
	}
}

//void feeder(){
//	char button = vexRT[Btn6U];
//	if(vexRT[Btn6D]){
//		motor[MID] = -button*127;
//	}
//	else{
//		motor[MID] = button*127;
//	}

//}

task main()
{
	while(true){
		tankDrive();
		//arcadeDrive();
		shooter();
		//feeder();
		}
}
